/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: DefineClass.cs
* Script Author: MinhLe 
* Created On: 7/10/2020 12:43:49 AM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Kind
{
    Normal = 0, UI = 1
}

[System.Serializable]
public class DataCell
{
    public Cell cell;
    public int score;
    public void Clear()
    {
        cell = null;
        score = 0;
    }
    public DataCell(Cell cell, int score)
    {
        this.cell = cell;
        this.score = score;
    }
}

[System.Serializable]
public class Data
{
    public BasePieces myPiece;
    public List<DataCell> redCells;
    public List<DataCell> yellowCells;
    public List<DataCell> greenCells;
    public Calculator calculator;

    public void ClearData()
    {
        redCells.Clear();
        yellowCells.Clear();
        greenCells.Clear();
    }
}

[System.Serializable]
public class ReturnValueCell
{
    public int dir;//return 0 mean not 
}
