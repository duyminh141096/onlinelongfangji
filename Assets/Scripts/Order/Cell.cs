﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Cell : MonoBehaviour, IPointerDownHandler
{
    public Image mOutlineImage;

    public int mId;

    public BasePieces mCurrentPiece;

    public Color normalColor;

    [HideInInspector]
    public Vector2Int mBoardPosition = Vector2Int.zero;

    [HideInInspector]
    public Board mBoard = null;

    [HideInInspector]
    public RectTransform mRectTransform = null;

    public void SetColor()
    {
        mOutlineImage.color = normalColor;
    }

    public void RemovePiece()
    {
        if (mCurrentPiece != null)
        {
            mCurrentPiece = null;
        }

    }

    public bool ContainPiece()
    {
        if (mCurrentPiece != null) return true;
        return false;
    }


    public void Setup(Vector2Int newBoardPosition, Board board, int id)
    {
        mBoardPosition = newBoardPosition;

        mBoard = board;

        mRectTransform = gameObject.GetComponent<RectTransform>();

        mId = id;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (GameManager.Instance.Mode == GameMode.Play)
        {
            GameController.instance.GetInfoFromCell(this);
            return;
        }
        if(GameManager.Instance.Mode == GameMode.Ready)
        {
            //send to setup
            SetupLongPhuong.Instance.CheckCell(this);
        }
    }
}
