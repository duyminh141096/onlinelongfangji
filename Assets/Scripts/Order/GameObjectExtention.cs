/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: GameObjectExtention.cs
* Script Author: MinhLe 
* Created On: 7/7/2020 12:14:50 AM*/
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class GameObjectExtention
{
    public static List<Cell> GetAllCell(this GameObject gameObject)
    {
        var list = gameObject.GetComponentsInChildren<Cell>().ToList();
        return list;
    }
}
