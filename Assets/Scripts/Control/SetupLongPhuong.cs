/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: SetupLongPhuong.cs
* Script Author: MinhLe 
* Created On: 5/12/2020 11:41:48 PM*/
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public enum StateSetup
{
    pick = 0, place = 1
}


[System.Serializable]
public class Items
{
    public bool use;
    public BasePieces item;
}
[System.Serializable]
public class Cells
{
    public bool use;
    public Cell cell;
    public int stt;
}
public class SetupLongPhuong : MonoBehaviour
{
    public static SetupLongPhuong Instance;
    private void Awake()
    {
        Instance = this;
    }

    public GameObject setupPanel;
    public GameObject placePanelX, placePanelC;

    public StateSetup state;

    public int itemP;
    public bool teamC;

    public GameObject panelC, panelX;
    public int openYC, openYX, closeYC, closeYX;

    public List<Items> ItemsC, ItemsX;
    public List<Cells> CellsC, CellsX;//7:7,1:7,7:1,1:1

    public List<GameObject> buttonItemC, buttonItemX;

    [Header("Arrow")]
    public List<GameObject> ArrowC, ArrowX;

    public void SwitchSide(bool team, bool show)
    {
        Show(team, show);
        state = StateSetup.pick;
    }
    public bool CheckStartGame()
    {
        bool start = true;
        for (int i = 0; i < ItemsX.Count; i++)
        {
            if (ItemsC[i].use == false || ItemsX[i].use == false)
                start = false;
        }
        return start;
    }
    public void ClickItem(int i)
    {
        itemP = i;
    }
    public void ClickTeam(bool team)
    {
        state = StateSetup.place;
        teamC = team;
        Show(teamC, false);
        if (teamC)
            placePanelX.SetActive(true);
        else
            placePanelC.SetActive(true);
    }
    public void AddItem(BasePieces initem, bool teamC)
    {
        Items a = new Items();
        a.use = false;
        a.item = initem;
        if (teamC)
            ItemsC.Add(a);
        else
            ItemsX.Add(a);
    }
    int sttC = -1;
    int sttX = -1;
    public void AddCell(Cell cell, bool team)
    {
        Cells a = new Cells();
        a.cell = cell;
        a.use = false;
        if (team) { sttC++; a.stt = sttC; }
        else { sttX++; a.stt = sttX; };
       
        if (team)
            CellsC.Add(a);
        else
            CellsX.Add(a);
    }
    public void CheckCell(Cell cell)
    {
        Cells check;
        if (teamC)
            check = CellsC.Find(x => x.cell == cell);
        else
            check = CellsX.Find(x => x.cell == cell);
        if (check != null && !check.use)
            Action(check);

    }

    public void Action(Cells cell)
    {
        if (state == StateSetup.place)
        {
            if (teamC)
            {
                ItemsC[itemP].item.Place(cell.cell);
                ItemsC[itemP].use = true;
                ArrowC[cell.stt].SetActive(false);
            }
            else
            {
                ItemsX[itemP].item.Place(cell.cell);
                ItemsX[itemP].use = true;
                ArrowX[cell.stt].SetActive(false);
            }
            cell.use = true;
            if (!CheckStartGame())
            {
                state = StateSetup.pick;
                SwitchSide(!teamC, true);
            }
            else
            {
                DeleteAll();
                GameManager.Instance.Mode = GameMode.Play;
            }
            placePanelX.SetActive(false);
            placePanelC.SetActive(false);
        }
    }

    private void DeleteAll()
    {
        Debug.Log("Delete all ");
        Destroy(setupPanel, 0.2f);
        Destroy(this, 0.2f);
    }
    /// <summary>
    /// show panel for user pickup
    /// </summary>
    /// <param name="show"></param>
    private void Show(bool team, bool show)
    {
        if (show)
        {
            if (team)
                panelC.transform.DOLocalMoveY(openYC, 0.5f);
            else
                panelX.transform.DOLocalMoveY(openYX, 0.5f);
        }
        else
        {
            if (team)
                panelC.transform.DOLocalMoveY(closeYC, 0.5f).OnComplete(OnComplete);
            else
                panelX.transform.DOLocalMoveY(closeYX, 0.5f).OnComplete(OnComplete);
        }
    }

    private void OnComplete()
    {
        if (teamC)
        {
            buttonItemC[itemP].SetActive(false);
            if (itemP == 0) buttonItemC[1].transform.localPosition = new Vector3(0, buttonItemC[1].transform.localPosition.y);
            else buttonItemC[0].transform.localPosition = new Vector3(0, buttonItemC[0].transform.localPosition.y);
        }
        else
        {
            buttonItemX[itemP].SetActive(false);
            if (itemP == 0) buttonItemX[1].transform.localPosition = new Vector3(0, buttonItemX[1].transform.localPosition.y);
            else buttonItemX[0].transform.localPosition = new Vector3(0, buttonItemX[0].transform.localPosition.y);
        }
    }
}


