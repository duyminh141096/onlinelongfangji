﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public enum State
{
    None, Ready
};

public class GameController : MonoBehaviour
{


    #region Control UI 


    //========= UI ===========
    //=======White Team ======
    [Header("--UI--")]
    public GameObject UI_Move;

    public GameObject UI_MoveandKill;

    //public GameObject ReadyImage;

    //public Animator Anim_UI_Ready;

    public GameObject UI_Rotation;

    public GameObject UI_Rotation_White;

    public GameObject UI_Rotation_Black;

    public GameObject UI_PassTurn_White;

    public Text Text_WinText;

    public Text TisoWin;

    public GameObject Panel_Win;

    public GameObject UI_Sur;

    public GameObject ImageRotation;

    public GameObject UI_Undo;

    public GameObject UI_Menu;

    public GameObject UI_Home;

    public GameObject Shield;


    //================================

    [Header("------")]
    public GameObject StartPanel;

    public static int GreenWin = 0;

    public static int OrangeWin = 0;

    public void Surrender()
    {
        if (PiecesManager.instance.isBlackTurn)
        {
            GreenWin++;
            WinGame("Player Two wins this round!");
        }
        else
        {
            OrangeWin++;
            WinGame("Player One wins this round!");
        }
    }

    public void ShieldGame()
    {
        Invoke("UnActiveShield", 0.51f);
    }
    void UnActiveShield()
    {
        Shield.SetActive(false);
    }

    public void ShowMenuUI(bool show)
    {
        UI_Menu.SetActive(show);
    }

    public void LoadSceneGame(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void UISurShow(bool isBlackTurn)
    {

        if (isBlackTurn)
        {
            UI_Sur.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
        }
        else
        {
            UI_Sur.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
        }
    }
    public void UI_PassTurn_White_Active(bool active)
    {
        SoundController.Instance.PlayVfxClip(3);
        if (mCurentBasePiece.mColor)
        {
            UI_PassTurn_White.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            UI_PassTurn_White.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
        }
        UI_PassTurn_White.SetActive(active);
        ImageRotation.SetActive(active);
    }

    public void TurnOnUIMove()
    {
        UI_Move.SetActive(true);
        if (mCurentBasePiece.mColor)
        {
            UI_Move.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            UI_Move.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
        }
    }

    public void ShowUIRotation(bool show)
    {

        if (mCurentBasePiece.mColor)
        {
            UI_Rotation.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            UI_Rotation.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
        }
        UI_Rotation.SetActive(show);
    }

    public void ChooseRotaOrpass(bool rotation)
    {
        SoundController.Instance.PlayVfxClip(3);
        if (rotation)
        {
            //show Ui Rotation
            ShowUIRotation(true, mCurentBasePiece.mColor);
            UI_PassTurn_White_Active(dropPiece);
        }
        else
        {
            UI_Rotation.SetActive(false);
            mCurentBasePiece.isDropPiece = false;
            ShowUIRotation(false, mCurentBasePiece.mColor);
            PiecesManager.instance.SwitchSides(!mCurentBasePiece.mColor);
        }
    }


    private void Awake()
    {
        PieceState = State.None;
        instance = this;

        mHightLightCells = new List<Cell>();

        #region For UI
        UI_Move.SetActive(false);

        UI_MoveandKill.SetActive(false);

        // ReadyImage.SetActive(true);

        UI_Undo.SetActive(false);

        UI_Home.SetActive(false);
        #endregion
    }
    private void Start()
    {
        StartPanel.SetActive(true);
    }

    public void ClickStartButton()
    {
        SoundController.Instance.PlayVfxClip(3);
        //Anim_UI_Ready.SetTrigger("Do_White");
        StartPanel.SetActive(false);
        SetupLongPhuong.Instance.SwitchSide(true, true);
    }


    public void ShowUIRotation(bool active, bool iswhitepieces)
    {

        if (active)
        {
            if (iswhitepieces) UI_Rotation_White.GetComponent<RectTransform>().DOAnchorPosY(0, 0.3f);
            else
                UI_Rotation_Black.GetComponent<RectTransform>().DOAnchorPosY(0, 0.3f);

            // UI_Undo.SetActive(false);

        }
        else
        {
            if (iswhitepieces) UI_Rotation_White.GetComponent<RectTransform>().DOAnchorPosY(1000, 0.3f);
            else
                UI_Rotation_Black.GetComponent<RectTransform>().DOAnchorPosY(1000, 0.3f);
            // UI_Undo.SetActive(true);
        }

    }
    public void TurnOffButtonRotate(bool active)
    {
        SoundController.Instance.PlayVfxClip(3);
        if (active)
        {
            UI_Rotation_White.GetComponent<RectTransform>().DOAnchorPosY(0, 0.3f);

            UI_Rotation_Black.GetComponent<RectTransform>().DOAnchorPosY(0, 0.3f);

        }
        else
        {
            UI_Rotation_White.GetComponent<RectTransform>().DOAnchorPosY(1000, 0.3f);

            UI_Rotation_Black.GetComponent<RectTransform>().DOAnchorPosY(1000, 0.3f);
        }

    }
    //bool isStart = true;
    #region  old Ready
    /*
       IEnumerator TuronOff()
       {
           yield return new WaitForSeconds(0.5f);
           if (isStart)
           {
               Anim_UI_Ready.SetTrigger("Do_Black");
               Anim_UI_Ready.SetBool("Ready_White", false);
           }
           else
           {

               Anim_UI_Ready.SetBool("Ready_Black", false);
           }

       }
       IEnumerator TurnOnImage()
       {
           yield return new WaitForSeconds(0.5f);
           ReadyImage.SetActive(false);

       }


       public void SwitchWhitePieces(bool isSwitches)
       {
           if (isSwitches)
           {
               //todo
               PiecesManager.instance.SwitchPieces(true);
               Anim_UI_Ready.SetBool("Ready_White", true);
               isStart = true;
               StartCoroutine("TuronOff");
           }
           else
           {
               Anim_UI_Ready.SetBool("Ready_White", true);
               isStart = true;
               StartCoroutine("TuronOff");
           }
       }
       public void SwitchBlackPieces(bool isSwitches)
       {
           if (isSwitches)
           {
               //todo
               PiecesManager.instance.SwitchPieces(false);
               Anim_UI_Ready.SetBool("Ready_Black", true);
               isStart = false;
               StartCoroutine("TuronOff");
           }
           else
           {
               Anim_UI_Ready.SetBool("Ready_Black", true);
               isStart = false;
               StartCoroutine("TuronOff");

           }
           StartCoroutine("TurnOnImage");
       }
       */
    #endregion

    #endregion

    #region For Rotation selected Item
    public BasePieces SelectedPiece;


    public void ChangeRotationRight()
    {
        SoundController.Instance.PlayVfxClip(3);
        int pastState = SelectedPiece.MStateRotation;

        if (!SelectedPiece.isDropPiece)
        {
            if (PiecesManager.rotation)
            {
                RotationPiece command = new RotationPiece(SelectedPiece, pastState);
                UndoManager.instance.InsertCommand(command);
                PiecesManager.rotation = false;
            }

            PiecesManager.instance.SwitchSides(!SelectedPiece.mColor);
            instance.ShowUIRotation(false, SelectedPiece.mColor);
            instance.UI_PassTurn_White_Active(false);
        }
        switch (SelectedPiece.MStateRotation)
        {
            case 1:
                ////Debug.Log("Change State Rotation");
                SelectedPiece.MStateRotation = 2;
                //Xoay hay lam gi do
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation);
                // mRightButton.onClick.RemoveAllListeners();
                break;
            case 2:
                SelectedPiece.MStateRotation = 3;
                //Xoay hay lam gi do
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation);
                // mRightButton.onClick.RemoveAllListeners();
                break;
            case 3:
                SelectedPiece.MStateRotation = 4;
                //Xoay hay lam gi do
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation);
                //  mRightButton.onClick.RemoveAllListeners();
                break;
            case 4:
                SelectedPiece.MStateRotation = 1;
                //Xoay hay lam gi do
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation);
                // mRightButton.onClick.RemoveAllListeners();
                break;
        }
        //teamuplist.UpdateRotation(BasePieces.MStateRotation);

        mCurentBasePiece.ClearCells();
        instance.mTargetCell = null;
        ClearListHightLightCell();
        SetState(State.None);
        SelectedCellColor(false);
        if (SelectedPiece.isDropPiece)
        {
            // mRightButton.onClick.RemoveAllListeners();
            //mRightButton.onClick.AddListener(ChangeRotationRight);
            //  mLeftButton.onClick.RemoveAllListeners();
            // mLeftButton.onClick.AddListener(ChangeRotationLeft);
        }

        ShowUIRotation(false);
    }

    public void ChangeRotationLeft()
    {
        SoundController.Instance.PlayVfxClip(3);
        int pastState = SelectedPiece.MStateRotation;

        if (!SelectedPiece.isDropPiece)
        {
            if (PiecesManager.rotation)
            {
                RotationPiece command = new RotationPiece(SelectedPiece, pastState);
                UndoManager.instance.InsertCommand(command);
                PiecesManager.rotation = false;
            }

            PiecesManager.instance.SwitchSides(!SelectedPiece.mColor);
            ShowUIRotation(false, SelectedPiece.mColor);
            //deactive 
            UI_PassTurn_White_Active(false);
        }

        switch (SelectedPiece.MStateRotation)
        {
            case 1:
                SelectedPiece.MStateRotation = 4;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation);
                //mLeftButton.onClick.RemoveAllListeners();
                break;
            case 2:
                SelectedPiece.MStateRotation = 1;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
            case 3:
                SelectedPiece.MStateRotation = 2;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
            case 4:
                SelectedPiece.MStateRotation = 3;
                SelectedPiece.GetandSetStateRotaion(SelectedPiece.MStateRotation);
                // mLeftButton.onClick.RemoveAllListeners();
                break;
        }

        //teamuplist.UpdateRotation(BasePieces.MStateRotation);

        mCurentBasePiece.ClearCells();
        SelectedCellColor(false);
        mTargetCell = null;
        ClearListHightLightCell();
        SetState(State.None);
        if (SelectedPiece.isDropPiece)
        {
            //mRightButton.onClick.RemoveAllListeners();
            //  mRightButton.onClick.AddListener(ChangeRotationRight);
            //  mLeftButton.onClick.RemoveAllListeners();
            // mLeftButton.onClick.AddListener(ChangeRotationLeft);
        }

        // GameController.instance.ShowUIRotation(false, BasePieces.mColor);
        ShowUIRotation(false);

    }


    #endregion

    public State PieceState;

    public static GameController instance;
    public int stt = 0;
    public List<Cell> mHightLightCells;
    public bool isMoveTeam = false;
    public static bool dropPiece;

    public BasePieces mCurentBasePiece;
    public BasePieces mTargetBasePiece;

    public Cell mCurrentCell;
    public Cell mTargetCell;

    [Header("Store Pieces")]
    public StorePieces StorePieces;

    public void ShowCell(BasePieces basePiece)
    {
        mCurentBasePiece = basePiece;
        PieceState = State.Ready;
        mCurentBasePiece.CheckPathing();
        mCurentBasePiece.ShowCells();

        mHightLightCells = mCurentBasePiece.mHightlightedCells;
        mCurrentCell = mCurentBasePiece.mCurrentCell;

        PiecesManager.rotation = true;

        UI_Undo.SetActive(false);
        UI_Home.SetActive(false);
    }

    public void CheckPiece(BasePieces piece)
    {
        if (piece.mColor == PiecesManager.instance.isBlackTurn)
        {
            if (PieceState == State.Ready)
            {
                if (mCurentBasePiece.mID == piece.mID)
                {
                    SelectedCellColor(false);
                    mCurentBasePiece.ClearCells();
                    mTargetCell = null;
                    ClearListHightLightCell();
                    PieceState = State.None;
                    ShowUIRotation(false, mCurentBasePiece.mColor);
                    SelectedPiece = null;
                    UI_Undo.SetActive(true);
                    UI_Home.SetActive(true);
                    StorePieces.Instance.ShowSelectCell(piece.transform, false);
                }
                else
                {
                    Store storae = StorePieces.GetInfo(piece.mNamePiece, piece.mColor);
                    if (storae.count > 0)
                    {
                        SelectedCellColor(false);
                        mCurentBasePiece.ClearCells();
                        mTargetCell = null;
                        ClearListHightLightCell();
                        ShowUIRotation(false, piece.mColor);
                        SelectedCellColor(false);
                        ShowCell(piece);
                        StorePieces.Instance.ShowSelectCell(piece.transform, true);
                    }
                    else
                    {
                        mCurentBasePiece.ClearCells();
                        SelectedCellColor(false);
                        PieceState = State.None;
                        ShowUIRotation(false, mCurentBasePiece.mColor);
                        SelectedPiece = null;
                        UI_Undo.SetActive(true);
                        UI_Home.SetActive(true);
                        StorePieces.Instance.ShowSelectCell(piece.transform, false);
                    }
                }
                return;
            }
            Store store = StorePieces.GetInfo(piece.mNamePiece, piece.mColor);
            if (store.count > 0)
            {
                if (PieceState == State.None)
                {
                    ShowCell(piece);
                    StorePieces.Instance.ShowSelectCell(piece.transform, true);
                }
            }
        }
    }

    public Color normalCellColor;
    public Color selectedCellColor;

    public Cell SelectedCell;

    public void SelectedCellColor(bool enable)
    {
        if (SelectedCell != null)
        {
            SelectedCell.mOutlineImage.enabled = enable;
            if (enable)
                SelectedCell.mOutlineImage.color = selectedCellColor;
            else
                SelectedCell.SetColor();
        }
    }


    public void GetInfoFromPiece(BasePieces basePiece)
    {
        if (PieceState == State.None)
        {
            if (basePiece.mColor == PiecesManager.instance.isBlackTurn)
            {
                ShowCell(basePiece);
                if (basePiece.mNamePiece == "Long" || basePiece.mNamePiece == "Phuong" || basePiece.mNamePiece == "Vuong" || basePiece.isDropPiece == true)
                {
                    SelectedCell = basePiece.mCurrentCell;
                    SelectedCellColor(true);
                    ShowUIRotation(false, basePiece.mColor);
                    return;
                }
                ShowUIRotation(true, basePiece.mColor);
                SelectedCell = basePiece.mCurrentCell;
                SelectedCellColor(true);
                SelectedPiece = basePiece;
                return;
            }
        }
        if (PieceState == State.Ready)
        {
            if (basePiece.mID == mCurentBasePiece.mID)
            {
                mCurentBasePiece.ClearCells();
                PieceState = State.None;
                ShowUIRotation(false, mCurentBasePiece.mColor);
                SelectedPiece = null;
                UI_Undo.SetActive(true);
                UI_Home.SetActive(true);
                SelectedCell = basePiece.mCurrentCell;
                SelectedCellColor(false);
            }
            else
            {
                if (basePiece.kind == Kind.Normal)
                {
                    if (basePiece.mColor == mCurentBasePiece.mColor) // team
                    {
                        bool checkMove = CheckContainCell(basePiece.mCurrentCell);
                        if (checkMove)//Piece nam trong cell hightlight
                        {
                            if (mCurentBasePiece.kind == Kind.Normal)
                            {
                                TeamUp command = new TeamUp();
                                command.GetStandPiece(basePiece);
                                command.GetMovePiece(mCurentBasePiece);
                                PiecesManager.rotation = false;
                                UndoManager.instance.InsertCommand(command);

                                mCurentBasePiece.TeamUp(basePiece.mListPiece.GetPieceNamesList());//teamup
                                mCurentBasePiece.Move(basePiece.mCurrentCell, true, false);//move
                                PieceState = State.None;
                                SelectedPiece = mCurentBasePiece;
                                Destroy(basePiece.gameObject, 0.4f);

                                string NamePiece = mCurentBasePiece.mNamePiece;
                                if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
                                {
                                    PiecesManager.instance.SwitchSides(!basePiece.mColor);
                                }
                                else
                                {
                                    ShowUIRotation(true);
                                    //PiecesManager.instance.SwitchSides(!basePiece.mColor);
                                    //UI_PassTurn_White_Active(true);
                                }
                                return;
                            }
                            if (mCurentBasePiece.kind == Kind.UI)
                            {
                                //create , team up , remove from store , add command
                                //assign command
                                TeamUpFormStoreToCell command = new TeamUpFormStoreToCell();
                                command.GetInfoOldPieces(basePiece, mCurentBasePiece.transform.position, basePiece.mColor);

                                //create new piece
                                StorePieces.Instance.ShowSelectCell(mCurentBasePiece.transform, false);
                                GameObject newPiece = PiecesManager.instance.CreateBasePiece(mCurentBasePiece.mColor, mCurentBasePiece.MStateRotation, mCurentBasePiece.mNamePiece);
                                newPiece.GetComponent<BasePieces>().isDropPiece = true;

                                command.GetInfoNewPiece(newPiece.GetComponent<BasePieces>().mNamePiece);
                                UndoManager.instance.InsertCommand(command);//done command

                                SelectedPiece = newPiece.GetComponent<BasePieces>();
                                newPiece.transform.position = mCurentBasePiece.transform.position;//done create

                                newPiece.GetComponent<BasePieces>().TeamUp(basePiece.mListPiece.GetPieceNamesList());//teamup
                                newPiece.GetComponent<BasePieces>().Move(basePiece.mCurrentCell, true, false);//move

                                SelectedPiece = newPiece.GetComponent<BasePieces>();
                                Destroy(basePiece.gameObject, 0.4f);

                                StorePieces.Instance.Remove(mCurentBasePiece.mNamePiece, mCurentBasePiece.mColor);//remove from store
                                SetState(State.None);
                                mCurentBasePiece.ClearCells();
                                ClearListHightLightCell();
                                SelectedCellColor(false);

                                mCurentBasePiece = newPiece.GetComponent<BasePieces>();

                                string NamePiece = mCurentBasePiece.mNamePiece;
                                if (NamePiece == "Long" || NamePiece == "Phuong" || NamePiece == "Vuong")
                                {
                                    PiecesManager.instance.SwitchSides(!basePiece.mColor);
                                }
                                else
                                {
                                    ShowUIRotation(true);
                                    //PiecesManager.instance.SwitchSides(!basePiece.mColor);
                                    //UI_PassTurn_White_Active(true);
                                }
                                return;
                            }
                        }
                        else //nam ngoai hight light cell cua mCurrentPiece
                        {
                            SelectedCellColor(false);
                            mCurentBasePiece.ClearCells();
                            mTargetCell = null;
                            ClearListHightLightCell();
                            ShowCell(basePiece);

                            if (basePiece.mNamePiece == "Long" || basePiece.mNamePiece == "Phuong"
                                || basePiece.mNamePiece == "Vuong" || basePiece.isDropPiece == true)
                                ShowUIRotation(false, basePiece.mColor);
                            else
                            {
                                SelectedPiece = basePiece;
                                ShowUIRotation(true, basePiece.mColor);
                            }
                            SelectedCell = basePiece.mCurrentCell;
                            SelectedCellColor(true);
                        }

                    }
                    else // khac team
                    {

                        mTargetCell = basePiece.mCurrentCell;
                        mTargetBasePiece = basePiece;

                        bool checkMove = CheckContainCell(mTargetCell);

                        if (checkMove)
                        {
                            TurnOffButtonRotate(false);
                            CheckMove(false);
                        }
                        else
                        {
                            mTargetCell = null;
                            mTargetBasePiece = null;
                        }
                    }
                }
            }
        }
    }

    public void GetInfoFromCell(Cell cell)
    {
        if (PieceState == State.Ready && cell.mCurrentPiece == null)
        {
            mTargetCell = cell;

            bool checkMove = CheckContainCell(mTargetCell);

            if (checkMove)
            {
                CheckMove(true);
            }
        }
    }

    public int GetStt()
    {
        stt++;
        return stt;
    }

    public State GetState()
    {
        return PieceState;
    }
    public void ClearState()
    {
        PieceState = State.None;
    }
    public void SetState(State state)
    {
        Debug.Log("Set State");
        PieceState = state;
        //if (PieceState == State.Ready) UISurShow(false);
        //else UISurShow(true);
    }

    public void CheckMove(bool isNotPiece)
    {
        if (isNotPiece)
        {
            if (mCurentBasePiece.mListPiece.GetListNameCount() > 1)
            {
                TurnOffButtonRotate(false);
                TurnOnUIMove();
            }
            else
            {
                if (mCurentBasePiece.kind == Kind.Normal)
                {
                    MoveSingleCommand command = new MoveSingleCommand(mCurentBasePiece, mTargetCell);
                    UndoManager.instance.InsertCommand(command);
                    PiecesManager.rotation = false;
                    mCurentBasePiece.Move(mTargetCell, false, false);
                    SetState(State.None);
                    return;
                }
                if (mCurentBasePiece.kind == Kind.UI)
                {
                    //add command
                    Store store = StorePieces.Instance.GetInfo(mCurentBasePiece.mNamePiece, mCurentBasePiece.mColor);
                    if (store != null)
                    {
                        MoveFromStoreToCell command = new MoveFromStoreToCell(store.pos.localPosition, mTargetCell, store.teamColor);
                        UndoManager.instance.InsertCommand(command);
                    }
                    PiecesManager.rotation = false;
                    StorePieces.Instance.ShowSelectCell(mCurentBasePiece.transform, false);
                    GameObject newPiece = PiecesManager.instance.CreateBasePiece(mCurentBasePiece.mColor, mCurentBasePiece.MStateRotation, mCurentBasePiece.mNamePiece);
                    newPiece.GetComponent<BasePieces>().isDropPiece = true;
                    SelectedPiece = newPiece.GetComponent<BasePieces>();
                    newPiece.transform.position = mCurentBasePiece.transform.position;
                    mCurentBasePiece.ClearCells();
                    SelectedCellColor(false);
                    mCurentBasePiece = newPiece.GetComponent<BasePieces>();
                    newPiece.GetComponent<BasePieces>().Move(mTargetCell, false, false);
                    StorePieces.Instance.Remove(mCurentBasePiece.mNamePiece, mCurentBasePiece.mColor);
                    SetState(State.None);
                    ClearListHightLightCell();
                    return;
                    //move
                }
            }
        }
        else
        {
            if (mCurentBasePiece.mListPiece.GetListNameCount() > 1)
            {
                if (mCurentBasePiece.mColor)
                {
                    UI_MoveandKill.SetActive(true);
                    UI_MoveandKill.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
                }
                else
                {
                    UI_MoveandKill.SetActive(true);
                    UI_MoveandKill.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
                }

            }
            else
            {
                MoveTeamandKill command = new MoveTeamandKill(mCurentBasePiece, mTargetCell);
                command.GetKilledPiece(mTargetBasePiece);
                mCurentBasePiece.Move(mTargetCell, false, true);
                SelectedPiece = mCurentBasePiece;
                mTargetBasePiece.Kill(mTargetBasePiece.mListPiece.GetPieceNamesList(), command);
                UndoManager.instance.InsertCommand(command);
                PiecesManager.rotation = false;
                //PiecesManager.instance.SwitchSides(!mCurentBasePiece.mColor);
                SetState(State.None);
            }
        }

    }

    public void WinGame(string winString)
    {
        SoundController.Instance.PlayVfxClip(1);
        Text_WinText.text = winString;
        TisoWin.text = OrangeWin + " - " + GreenWin;
        if (PiecesManager.instance.isBlackTurn)
        {
            Panel_Win.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            Panel_Win.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
        }

        Panel_Win.SetActive(true);
    }



    public void PassTurn()
    {
        PiecesManager.instance.SwitchSides(!mCurentBasePiece.mColor);
        mCurentBasePiece.isDropPiece = false;
        ShowUIRotation(false, mCurentBasePiece.mColor);
    }

    public void CheckMoveTeam(bool check)
    {
        if (check)
        {
            mCurentBasePiece.MoveTeam(mTargetCell);
        }
        else
        {
            mCurentBasePiece.MoveOnly(mTargetCell);
        }
    }
    public void CheckMoveandKill(bool check)
    {
        if (check)
        {
            mCurentBasePiece.MoveTeamAndKill(mTargetCell);
        }
        else
        {
            mCurentBasePiece.MoveOnlyAndKill(mTargetCell);
        }
    }
    public bool CheckContainCell(Cell mTargetCell)
    {
        if (mHightLightCells != null && mTargetCell != null)
        {
            for (int i = 0; i < mHightLightCells.Count; i++)
            {
                if (mHightLightCells[i].mId == mTargetCell.mId)
                {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public void ClearListHightLightCell()
    {
        mHightLightCells.Clear();
    }

}
