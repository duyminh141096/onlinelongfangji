﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ControllRules : MonoBehaviour
{
    public RectTransform englishRules;

    public RectTransform vietnameseRules;

    public ScrollRect ScrollRect;

    public RectTransform startPos;

    public float duration;
  
    public void EnglishRule()
    {
        vietnameseRules.gameObject.SetActive(false);
        englishRules.gameObject.SetActive(true);
        ScrollRect.content = englishRules;
    }

    public void VietnameseRule()
    {
        vietnameseRules.gameObject.SetActive(true);
        englishRules.gameObject.SetActive(false);
        ScrollRect.content = vietnameseRules;
    }

    public void ShowPanelRules()
    {
        startPos.DOAnchorPosY(0, duration);
    }
    public void HidePanelRules()
    {
        startPos.DOAnchorPosY(1200, duration);
    }

}
