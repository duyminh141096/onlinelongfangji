/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: BasePieces.bot.cs
* Script Author: MinhLe 
* Created On: 7/10/2020 12:26:58 AM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class BasePieces
{
    public int score;

    public void CheckPathifPieceisUI()
    {
        for (int y = 0; y < 9; y++)
        {
            for (int x = 0; x < 9; x++)
            {
                CellState cellState = CellState.None;
                cellState = Board.instance.ValidateCell(x, y, this);

                if (cellState == CellState.Friendly || cellState == CellState.Free)
                {
                    mHightlightedCells.Add(Board.instance.AllCells[x, y]);
                }
            }
        }
    }
    /* 
     Nhiem vu hom nay 10/7
    
         */


    /// <summary>
    /// Nhiem vu cua ham nay la
    /// Check lam sao o con cell nay voi quan co` nay thi di quan nao va huong xoay nao se an duoc co` vao nuoc sau (check ca 4 huong)
    /// tra ve 1 dir #define dir : 0 : khong co huong nao , 1,2,3,4 huong can xoay
    /// </summary>
    public int CheckPathingAtCell(Cell targetCell)
    {
        //if (kind == Kind.UI)
        //{
        //    CheckPathifPieceisUI();
        //    return 0;
        //}
        switch (mNamePiece)
        {
            case "Binh":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:
                            CreateCellPath(0, -1, Mathf.Abs(mMovement.y));
                            break;
                        case 2:
                            CreateCellPath(-1, 0, Mathf.Abs(mMovement.y));
                            break;
                        case 3:
                            CreateCellPath(0, 1, Mathf.Abs(mMovement.y));
                            break;
                        case 4:
                            CreateCellPath(1, 0, Mathf.Abs(mMovement.y));
                            break;
                        default:
                            break;
                    }
                }
                break;
            case "Ki":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:
                            CreatePathKi(3, -1, -2);
                            CreatePathKi(3, 1, -2);
                            CreatePathKi(3, 0, 2);
                            CreatePathKi(3, 2, 2);
                            CreatePathKi(3, -2, 2);
                            break;
                        case 2:
                            CreatePathKi(4, -2, -1);
                            CreatePathKi(4, -2, 1);
                            CreatePathKi(4, 2, 0);
                            CreatePathKi(4, 2, -2);
                            CreatePathKi(4, 2, 2);
                            break;
                        case 3:
                            CreatePathKi(1, -1, 2);
                            CreatePathKi(1, 1, 2);
                            CreatePathKi(1, -2, -2);
                            CreatePathKi(1, 2, -2);
                            CreatePathKi(1, 0, -2);
                            break;
                        case 4:
                            CreatePathKi(2, 2, 1);
                            CreatePathKi(2, 2, -1);
                            CreatePathKi(2, -2, 0);
                            CreatePathKi(2, -2, 2);
                            CreatePathKi(2, -2, -2);
                            break;
                    }
                }
                break;
            case "Phuong":
                if (mIsPlace)
                {
                    CreateCellPath(1, 0, mMovement.x);
                    CreateCellPath(-1, 0, mMovement.x);

                    CreateCellPath(0, 1, mMovement.y);
                    CreateCellPath(0, -1, mMovement.y);

                    CreateCellPath(-1, 1, mMovement.z);

                    CreateCellPath(1, 1, mMovement.z);

                    CreateCellPath(-1, -1, mMovement.z);


                    CreateCellPath(1, -1, mMovement.z);
                }
                break;
            case "Long":
                if (mIsPlace)
                {
                    CreateCellPath(1, 0, mMovement.x);
                    CreateCellPath(-1, 0, mMovement.x);

                    //vertical
                    CreateCellPath(0, 1, mMovement.y);
                    CreateCellPath(0, -1, mMovement.y);


                    CreateCellPath(-1, 1, mMovement.z);

                    CreateCellPath(1, 1, mMovement.z);


                    CreateCellPath(-1, -1, mMovement.z);


                    CreateCellPath(1, -1, mMovement.z);
                }
                break;
            case "Tuong":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:

                            CreateCellPath(1, 0, mMovement.x);
                            CreateCellPath(-1, 0, mMovement.x);


                            CreateCellPath(0, 1, mMovement.y);
                            CreateCellPath(0, -1, mMovement.y);


                            CreateCellPath(-1, -1, mMovement.z);


                            CreateCellPath(1, -1, mMovement.z);

                            break;
                        case 2:
                            CreateCellPath(1, 0, mMovement.x);
                            CreateCellPath(-1, 0, mMovement.x);

                            //vertical
                            CreateCellPath(0, 1, mMovement.y);
                            CreateCellPath(0, -1, mMovement.y);


                            CreateCellPath(-1, 1, mMovement.z);


                            CreateCellPath(-1, -1, mMovement.z);

                            break;
                        case 3:


                            CreateCellPath(1, 0, mMovement.x);
                            CreateCellPath(-1, 0, mMovement.x);
                            CreateCellPath(0, 1, mMovement.y);
                            CreateCellPath(0, -1, mMovement.y);
                            CreateCellPath(-1, 1, mMovement.z);
                            CreateCellPath(1, 1, mMovement.z);
                            break;
                        case 4:

                            CreateCellPath(1, 0, mMovement.x);
                            CreateCellPath(-1, 0, mMovement.x);

                            //vertical
                            CreateCellPath(0, 1, mMovement.y);
                            CreateCellPath(0, -1, mMovement.y);


                            CreateCellPath(1, 1, mMovement.z);


                            CreateCellPath(1, -1, mMovement.z);
                            break;
                    }
                }
                break;
            case "Si":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:


                            CreateCellPath(0, -1, mMovement.y);

                            //duong cheo tren trai
                            CreateCellPath(-1, 1, mMovement.z);
                            //duong cheo tren phai
                            CreateCellPath(1, 1, mMovement.z);

                            //duong cheo duoi trai
                            CreateCellPath(-1, -1, mMovement.z);

                            //duong cheo duoi phai
                            CreateCellPath(1, -1, mMovement.z);
                            break;
                        case 2:

                            CreateCellPath(-1, 0, mMovement.x);

                            //duong cheo tren trai
                            CreateCellPath(-1, 1, mMovement.z);
                            //duong cheo tren phai
                            CreateCellPath(1, 1, mMovement.z);

                            //duong cheo duoi trai
                            CreateCellPath(-1, -1, mMovement.z);

                            //duong cheo duoi phai
                            CreateCellPath(1, -1, mMovement.z);
                            break;
                        case 3:

                            CreateCellPath(0, 1, mMovement.y);

                            //duong cheo tren trai
                            CreateCellPath(-1, 1, mMovement.z);
                            //duong cheo tren phai
                            CreateCellPath(1, 1, mMovement.z);

                            //duong cheo duoi trai
                            CreateCellPath(-1, -1, mMovement.z);

                            //duong cheo duoi phai
                            CreateCellPath(1, -1, mMovement.z);
                            break;
                        case 4:


                            CreateCellPath(-1, 1, mMovement.z);

                            CreateCellPath(1, 1, mMovement.z);


                            CreateCellPath(-1, -1, mMovement.z);


                            CreateCellPath(1, -1, mMovement.z);

                            CreateCellPath(1, 0, mMovement.z);
                            break;

                    }
                }
                break;
            case "Vuong":
                if (mIsPlace)
                {
                    CreateCellPath(1, 0, mMovement.x);
                    CreateCellPath(-1, 0, mMovement.x);


                    CreateCellPath(0, 1, mMovement.y);
                    CreateCellPath(0, -1, mMovement.y);


                    CreateCellPath(-1, 1, mMovement.z);

                    CreateCellPath(1, 1, mMovement.z);

                    CreateCellPath(-1, -1, mMovement.z);

                    CreateCellPath(1, -1, mMovement.z);
                }
                break;
            case "Xa":
                if (mIsPlace)
                {
                    switch (MStateRotation)
                    {
                        case 1:
                            CreateCellPath(0, -1, Mathf.Abs(mMovement.y));
                            break;
                        case 2:
                            CreateCellPath(-1, 0, Mathf.Abs(mMovement.y));
                            break;
                        case 3:
                            CreateCellPath(0, 1, Mathf.Abs(mMovement.y));
                            break;
                        case 4:
                            CreateCellPath(1, 0, Mathf.Abs(mMovement.y));
                            break;
                        default:
                            break;
                    }
                }
                break;
        }
        return 0;
    }


}
