﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PieceInfo
{
    public string nPieceName;

    public int mPieceRotation;

    public PieceInfo(string mpiecename, int mpiecesrotation)
    {
        nPieceName = mpiecename;

        mPieceRotation = mpiecesrotation;
    }
}



public class TeamUpList : MonoBehaviour
{

    public int score; 

    public BasePieces basePieces;

    public List<PieceInfo> mPiecesInfo = new List<PieceInfo>();


    public void UpdateScore() {
        score = 0;
        if(mPiecesInfo.Count > 0)
        {
            foreach (var item in mPiecesInfo)
            {
                score += ScorePiece.instance.GetScorePiece(item.nPieceName);
            }
        }
    }

    public List<PieceInfo> GetPieceNamesList()
    {
        return mPiecesInfo;
    }

    void Start()
    {
        basePieces = gameObject.GetComponent<BasePieces>();
        UpdateScore();
        //AddPiece(basePieces.mNamePiece, basePieces.MStateRotation);
    }

    public void AddPiece(string pieceName,int stateRotation)
    {
 
        if (pieceName != null)
        {
            mPiecesInfo.Insert(0, new PieceInfo(pieceName,stateRotation));
            UpdateScore();
        }
    }

    public void AddPiecesList(List<PieceInfo> listPieces)
    {
        foreach (PieceInfo piece in listPieces)
        {
            if (piece != null)
            {
                mPiecesInfo.Add(piece);
            }
        }
        UpdateScore();
    }


    public void RemovePieceList()
    {
        mPiecesInfo.RemoveAt(0);
        UpdateScore();
    }

    public void UpdateRotation(int rotation)
    {
        mPiecesInfo[0].mPieceRotation = rotation;
    }

    public string GetPieceList()
    {
        return mPiecesInfo[0].nPieceName;
    }

    public int GetPieceListRotation()
    {
        return mPiecesInfo[0].mPieceRotation;
    }

    public int GetListNameCount()
    {
        return mPiecesInfo.Count;
    }


}
