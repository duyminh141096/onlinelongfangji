/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: ControllerBot.cs
* Script Author: MinhLe 
* Created On: 7/6/2020 10:16:07 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControllerBot : MonoBehaviour
{
    public List<Data> green;
    public List<Data> orange;
    public  List<Cell> cells;
    public GameObject parentCells;
    public static ControllerBot Instance;
    private void Awake()
    {
        Instance = this;
        green = new List<Data>();
        orange = new List<Data>();
    }
    public void Add(Calculator returnPiece , bool greenTeam)
    {
        if (greenTeam)
            green.Add(returnPiece.myData);
        else
            orange.Add(returnPiece.myData);
    }
    public void Remove(Calculator returnPiece, bool greenTeam)
    {
        if (greenTeam)
            green.RemoveAt(green.FindIndex(x => x == returnPiece.myData));
        else
            orange.RemoveAt(orange.FindIndex(x => x == returnPiece.myData));
    }
    public void CallCaculate()
    {
        StartCoroutine(Call(green));
        StartCoroutine(Call(orange));
    }

    private IEnumerator Call(List<Data> targetList)
    {
        foreach (Data item in targetList)
        {
            StartCoroutine(item.calculator.Calculate());
        }
        yield return null;
    }

    public void GetAllCell()
    {
        cells.Clear();
        cells = parentCells.GetAllCell();
    }

    public void AttachCell()
    {

    }


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Call ~~~~~~~~~~~~~~~~~~~~~~~~");
            CallCaculate();
        }
    }
}
