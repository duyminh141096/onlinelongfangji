/*Project Name : Cờ Long Phượng - Long Fang Ji
* Script Name: Calculator.cs
* Script Author: MinhLe 
* Created On: 7/6/2020 10:16:39 PM*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Calculator : MonoBehaviour
{
    public BasePieces myPiece;
    public Data myData;
    private void Start()
    {
        myPiece = GetComponent<BasePieces>();
        myData.calculator = this;
        ControllerBot.Instance.Add(this, myPiece.mColor);
    }
    public IEnumerator Calculate()
    {
        BasePieces clone = (BasePieces)myPiece.Clone();
        myData.ClearData();
        if (clone.kind == Kind.Normal)
        {
            clone.CheckPathing();
            Debug.Log("clone ~~ Piece name : " + clone.mNamePiece + " team color : "+ clone.mColor +  " ~~ my list cell count after checking : " + clone.mHightlightedCells.Count);
            // ok => next . check every cell 
            CheckCell(clone.mHightlightedCells, clone);


            yield return null;
        }
    }
    private void CheckCell(List<Cell> targetCells ,BasePieces myPiece)
    {
        //kiem tra xem con co` trong cell hien tai voi con co` trong cell target xem co trung` mau` k
        //neu trung mau thi check path con co` do neu con co` hien tai dung tren con cell do (quay ve 4 huong an duoc 1 con co` dich thi la yellow piece , neu khong thi la red piece)
        foreach (Cell item in targetCells)
        {
            if(item.mCurrentPiece.mColor == myPiece.mColor)// chung team
            {
                //phan loai xem la red piece hay yellow
                continue;
            }
            if (item.mCurrentPiece.mColor != myPiece.mColor)// khac team
            {
                myData.greenCells.Add(new DataCell(item, item.mCurrentPiece.score));
                continue;
            }

        }
    }
    /*
     * update ngay 10/7
     * lam toi phan check path , phan loai duoc green cell roi , gio can phan loai ra red cell va yellow cell
     * 
     * */
}
