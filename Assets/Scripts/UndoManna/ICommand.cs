﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICommand 
{
    void Undo();
}

public interface IKillCommand
{
    void GetCell(Cell cell,string name);
}

