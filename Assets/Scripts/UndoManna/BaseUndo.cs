﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUndo 
{
    Stack<ICommand> undoCommand = new Stack<ICommand>();

    public void Undo()
    {
        if(undoCommand.Count > 0)
        {
            //pop
            ICommand command = undoCommand.Pop();
            command.Undo();

            //Debug.Log("_____Undo_________");
        }
    }

    public void Clear()
    {
        undoCommand.Clear();
    }

    public void InsertCommand(ICommand command)
    {
        if(undoCommand.Count >= 0)
        {
            undoCommand.Push(command);
        }
    }
}
