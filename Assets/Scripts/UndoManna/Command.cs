﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class MoveSingleCommand : ICommand
{

    /*name
     * team color 
     * current cell
     * rotation state
     * */
    string mPieceName;

    bool mTeamColor;

    Cell mPieceCell;

    int mStateRotaion;

    bool isPlace;

    bool isDropPiece;

    List<PieceInfo> mListPieceInfo = new List<PieceInfo>();

    Cell currentCell;

    public MoveSingleCommand(BasePieces piece, Cell currentCell)
    {

        mPieceName = piece.mNamePiece;

        mTeamColor = piece.mColor;

        mPieceCell = piece.mCurrentCell;

        mStateRotaion = piece.MStateRotation;

        isPlace = piece.mIsPlace;
        //Debug.Log("IsPlace at at command : " + isPlace);

        isDropPiece = piece.isDropPiece;

        //mListPieceInfo = piece.mListPiece.GetPieceNamesList();
        mListPieceInfo.AddRange(piece.mListPiece.GetPieceNamesList());

        this.currentCell = currentCell;

        //Debug.Log("Add MoveSingleCommand Command");
    }

    public void Undo()
    {

        if (mPieceCell.mCurrentPiece == null)
        {
            if (currentCell.mCurrentPiece.mNamePiece == mPieceName)
            {
                mPieceCell.mCurrentPiece = currentCell.mCurrentPiece;

                mPieceCell.mCurrentPiece.mCurrentCell = mPieceCell;

                mPieceCell.mCurrentPiece.GetandSetStateRotaion(mStateRotaion);

                mPieceCell.mCurrentPiece.transform.position = mPieceCell.transform.position;
                //Debug.Log("IsPlace at at undo : " + isPlace); 
                if (!isPlace)
                {
                    mPieceCell.mCurrentPiece.transform.SetParent(mPieceCell.transform);
                    mPieceCell.mCurrentPiece.transform.localScale = Vector3.one;
                }
                mPieceCell.mCurrentPiece.isDropPiece = isDropPiece;

                mPieceCell.mCurrentPiece.mIsPlace = isPlace;

                currentCell.mCurrentPiece = null;

               
                PiecesManager.instance.SwitchSides(mTeamColor);
            }

        }


    }
}

public class MoveSingleinTeam : ICommand
{
    string standPieceName;

    bool mTeamColor;

    Cell standCell;

    List<PieceInfo> standListPieceInfo = new List<PieceInfo>();

    string movePieceName;

    Cell moveCell;

    int moveStateRotation;

    public void GetMovePiece(BasePieces piece)
    {
        movePieceName = piece.mNamePiece;

        moveCell = piece.mCurrentCell;

        //moveListPieceInfo = piece.mListPiece.GetPieceNamesList();

        moveStateRotation = piece.MStateRotation;
    }

    public void GetStandPiece(BasePieces piece)
    {

        standPieceName = piece.mNamePiece;

        mTeamColor = piece.mColor;

        standCell = piece.mCurrentCell;

        standListPieceInfo.AddRange(piece.mListPiece.mPiecesInfo);
        //Debug.Log("Stand List : " + standListPieceInfo.Count);
        //standListPieceInfo = piece.mListPiece.GetPieceNamesList();

    }

    public void Undo()
    {
        if (moveCell.mCurrentPiece.mNamePiece == movePieceName)
        {

            moveCell.mCurrentPiece.Destroyyourself(0.1f);

            moveCell.mCurrentPiece = null;

            standCell.mCurrentPiece.Destroyyourself(0.1f);

            standCell.mCurrentPiece = null;

            GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(mTeamColor, moveStateRotation, movePieceName);

            newOrderPiece.GetComponent<TeamUpList>().AddPiecesList(standListPieceInfo);

            if (newOrderPiece.GetComponent<BasePieces>() != null)
            {
                newOrderPiece.GetComponent<BasePieces>().Place(standCell);
            }
            PiecesManager.instance.SwitchSides(mTeamColor);
        }

    }
}

public class MoveTeam : ICommand
{
    string namePiece;

    int stateRotation;

    bool teamColor;

    Cell pastCell;

    Cell presentCell;

    public MoveTeam(BasePieces piece, Cell targetCell)
    {
        namePiece = piece.mNamePiece;

        stateRotation = piece.MStateRotation;

        teamColor = piece.mColor;

        pastCell = piece.mCurrentCell;

        presentCell = targetCell;
    }

    public void Undo()
    {
        if (pastCell.mCurrentPiece == null)
        {
            presentCell.mCurrentPiece.transform.position = pastCell.transform.position;

            pastCell.mCurrentPiece = presentCell.mCurrentPiece;

            pastCell.mCurrentPiece.mCurrentCell = pastCell;

            presentCell.mCurrentPiece = null;

            pastCell.mCurrentPiece.GetandSetStateRotaion(stateRotation);

            PiecesManager.instance.SwitchSides(teamColor);

        }

    }
}
[System.Serializable]
public class StoreCell
{
    public Cell cell;
    public string namePieceinCell;
    public StoreCell(Cell cell, string name)
    {
        this.cell = cell;
        namePieceinCell = name;
    }
    public StoreCell()
    {
        cell = null;
        namePieceinCell = "";
    }
}

public class MoveSingleandKill : ICommand, IKillCommand
{

    bool teamColor;

    int movePieceRotation;

    string movePieceName;

    Cell moveCell;

    List<PieceInfo> standList = new List<PieceInfo>();

    Cell standCell;

    string killedPieceName;

    List<PieceInfo> killedList = new List<PieceInfo>();

    int killedPieceRotation;

    List<StoreCell> dropCell = new List<StoreCell>();

    public void GetMovePiece(BasePieces piece)
    {
        teamColor = piece.mColor;

        movePieceRotation = piece.MStateRotation;

        movePieceName = piece.mNamePiece;

        moveCell = piece.mCurrentCell;
    }

    public void GetStandPiece(BasePieces piece)
    {
        standCell = piece.mCurrentCell;

        standList.AddRange(piece.mListPiece.mPiecesInfo);
    }

    public void GetKilledPiece(BasePieces piece)
    {
        killedPieceName = piece.mNamePiece;

        killedPieceRotation = piece.MStateRotation;

        killedList.AddRange(piece.mListPiece.mPiecesInfo);
    }

    public void GetCell(Cell cell, string name)
    {
       
        StoreCell newcell = new StoreCell(cell, name);
        dropCell.Add(newcell);
    }

    public void Undo()
    {
        if (moveCell.mCurrentPiece.mNamePiece == movePieceName)
        {
            moveCell.mCurrentPiece.Destroyyourself(0.1f);

            moveCell.mCurrentPiece = null;

            standCell.mCurrentPiece.Destroyyourself(0.1f);

            standCell.mCurrentPiece = null;

            GameObject newOrderPiece = PiecesManager.instance.CreateBasePiece(teamColor, movePieceRotation, movePieceName);

            newOrderPiece.GetComponent<TeamUpList>().AddPiecesList(standList);

            if (newOrderPiece.GetComponent<BasePieces>() != null)
            {
                newOrderPiece.GetComponent<BasePieces>().Place(standCell);
            }
            //re spawn dead chess
            GameObject killedPiece = PiecesManager.instance.CreateBasePiece(!teamColor, killedPieceRotation, killedPieceName);

            foreach (var item in killedList)
            {
                StorePieces.Instance.Remove(item.nPieceName, teamColor);
            }
            killedList.RemoveAt(0);

            killedPiece.GetComponent<TeamUpList>().AddPiecesList(killedList);

            killedPiece.GetComponent<BasePieces>().Place(moveCell);

            //destroy drop pieces
            
            foreach (StoreCell item in dropCell)
            {
                if(item.cell != null) item.cell.mCurrentPiece.Destroyyourself(0.1f);
               
            }

            PiecesManager.instance.SwitchSides(teamColor);
        }
    }
}

public class MoveTeamandKill : ICommand, IKillCommand
{
    string namePiece;

    int stateRotation;

    bool teamColor;

    Cell pastCell;

    Cell presentCell;

    string killedPieceName;

    List<PieceInfo> killedList = new List<PieceInfo>();

    int killedPieceRotation;

    List<StoreCell> dropCell = new List<StoreCell>();

    public MoveTeamandKill(BasePieces piece, Cell targetCell)
    {
        namePiece = piece.mNamePiece;

        stateRotation = piece.MStateRotation;

        teamColor = piece.mColor;

        pastCell = piece.mCurrentCell;

        presentCell = targetCell;
    }

    public void GetKilledPiece(BasePieces piece)
    {
        killedPieceName = piece.mNamePiece;

        killedPieceRotation = piece.MStateRotation;

        killedList.AddRange(piece.mListPiece.mPiecesInfo);
    }

    public void Undo()
    {
        if (pastCell.mCurrentPiece == null)
        {
            presentCell.mCurrentPiece.transform.position = pastCell.transform.position;

            pastCell.mCurrentPiece = presentCell.mCurrentPiece;

            pastCell.mCurrentPiece.mCurrentCell = pastCell;

            presentCell.mCurrentPiece = null;

            pastCell.mCurrentPiece.GetandSetStateRotaion(stateRotation);

            //re spawn
            GameObject killedPiece = PiecesManager.instance.CreateBasePiece(!teamColor, killedPieceRotation, killedPieceName);
            foreach (var item in killedList)
            {
                StorePieces.Instance.Remove(item.nPieceName, teamColor);
            }
            killedList.RemoveAt(0);

            killedPiece.GetComponent<TeamUpList>().AddPiecesList(killedList);

            killedPiece.GetComponent<BasePieces>().Place(presentCell);


            foreach (StoreCell item in dropCell)
            {
                if (item.cell != null) item.cell.mCurrentPiece.Destroyyourself(0.1f);
               // StorePieces.intance.RemoveStoredPiece(item.namePieceinCell, teamColor);
            }

            PiecesManager.instance.SwitchSides(teamColor);

        }

    }

    public void GetCell(Cell cell, string name)
    {
        StoreCell newcell = new StoreCell(cell, name);
        dropCell.Add(newcell);
    }

}

public class RotationPiece : ICommand
{
    bool teamColor;

    string pieceName;

    int stateRotation;

    Cell cell;

    public RotationPiece(BasePieces piece, int state)
    {
        pieceName = piece.mNamePiece;

        stateRotation = state;

        //Debug.Log("State rotation in commmand : " + stateRotation);

        cell = piece.mCurrentCell;

        teamColor = piece.mColor;
    }

    public void Undo()
    {
        if (cell.mCurrentPiece.mNamePiece == pieceName)
        {
            cell.mCurrentPiece.GetandSetStateRotaion(stateRotation);

            //cell.mCurrentPiece.mListPiece.UpdateRotation(stateRotation);

            PiecesManager.instance.SwitchSides(teamColor);
        }
    }
}

public class TeamUp : ICommand
{
    string standPieceName;

    int standRotation;

    bool teamColor;

    Cell standCell;

    bool isPlace;

    bool isDropPiece;

    List<PieceInfo> standList = new List<PieceInfo>();

    List<PieceInfo> moveList = new List<PieceInfo>();

    int moveRotaion;

    Cell moveCell;

    public void GetMovePiece(BasePieces piece)
    {
        moveRotaion = piece.MStateRotation;

        moveCell = piece.mCurrentCell;

        isPlace = piece.mIsPlace;
        //Debug.Log("ISPLACE IN TEAMUP : " + isPlace);
        isDropPiece = piece.isDropPiece;
        //moveList = piece.mListPiece.mPiecesInfo;

        moveList.AddRange(piece.mListPiece.GetPieceNamesList());
        
    }

    public void GetStandPiece(BasePieces piece)
    {

        standPieceName = piece.mNamePiece;

        teamColor = piece.mColor;

        standCell = piece.mCurrentCell;

        // standList = piece.mListPiece.GetPieceNamesList();
        standList.AddRange(piece.mListPiece.GetPieceNamesList());

        standRotation = piece.MStateRotation;

    }
    public void Undo()
    {

        standCell.mCurrentPiece.mListPiece.mPiecesInfo.Clear();
        standCell.mCurrentPiece.mListPiece.AddPiecesList(moveList);
        standCell.mCurrentPiece.transform.position = moveCell.transform.position;

        //Debug.Log("ISPLACE IN UNDO TEAM UP : " + isPlace);
        if (!isPlace)
        {
            standCell.mCurrentPiece.transform.SetParent(moveCell.transform);
            standCell.mCurrentPiece.transform.localScale = Vector3.one;
        }

        standCell.mCurrentPiece.GetandSetStateRotaion(moveRotaion);
        moveCell.mCurrentPiece = standCell.mCurrentPiece;
        moveCell.mCurrentPiece.mCurrentCell = moveCell;
        moveCell.mCurrentPiece.isDropPiece = isDropPiece;
        moveCell.mCurrentPiece.mIsPlace = isPlace;
        standCell.mCurrentPiece = null;
        GameObject newstandPiece = PiecesManager.instance.CreateBasePiece(teamColor, standRotation, standPieceName);
        newstandPiece.GetComponent<BasePieces>().Place(standCell);

        if (standList.Count >= 2)
        {
            standList.RemoveAt(0);
            newstandPiece.GetComponent<BasePieces>().mListPiece.AddPiecesList(standList);
        }
        PiecesManager.instance.SwitchSides(teamColor);

    }
}

public class MoveFromStoreToCell :  ICommand
{
    public Vector3 pos;
    public bool teamColor;
    public Cell cell;

    public MoveFromStoreToCell(Vector3 pos, Cell cell, bool teamColor)
    {
        this.cell = cell;
        this.pos = pos;
        this.teamColor = teamColor;
    }

    public void Undo()
    {
        //return to old pos , disable, add value to  store;
        BasePieces pieces = cell.mCurrentPiece;
        pieces.transform.DOLocalMove(pos, 0.3f);
        StorePieces.Instance.Add(pieces.mNamePiece, teamColor);
        pieces.gameObject.SetActive(false);
        pieces.Destroyyourself(0.1f);
        cell.mCurrentPiece = null;
        PiecesManager.instance.SwitchSides(teamColor);
        
    }
}

public class TeamUpFormStoreToCell : ICommand
{
    string oldPieceName;
    int oldPieceRotation;
    bool teamColor;
    bool isPlace;
    List<PieceInfo> standList = new List<PieceInfo>();
    public Vector3 pos;
    public Cell cell;
    string newpiece;
    public void GetInfoOldPieces(BasePieces oldpieces,Vector3 pos, bool teamColor)
    {
        oldPieceName = oldpieces.mNamePiece;
        standList = oldpieces.mListPiece.GetPieceNamesList();
        oldPieceRotation = oldpieces.MStateRotation;
        isPlace = oldpieces.mIsPlace;
        this.pos = pos;
        this.teamColor = teamColor;
        cell = oldpieces.mCurrentCell;
    }
    public void GetInfoNewPiece(string name)
    {
        newpiece = name;
    }

    public void Undo()
    {
        Debug.Log("Undo TeamUpFormStoreToCell");
        StorePieces.Instance.Add(newpiece, teamColor);
        if (cell.mCurrentPiece != null)
        {
            cell.mCurrentPiece.Destroyyourself(0.1f);
        }
        BasePieces newPiece = PiecesManager.instance.CreateBasePiece(teamColor, oldPieceRotation, oldPieceName).GetComponent<BasePieces>();
        standList.RemoveAt(0);
        newPiece.mListPiece.AddPiecesList(standList);
        newPiece.mIsPlace = isPlace;
        newPiece.Place(cell);
        PiecesManager.instance.SwitchSides(teamColor);
    }
}


//=======

